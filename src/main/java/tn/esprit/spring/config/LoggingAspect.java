package tn.esprit.spring.config;

import java.util.logging.LogManager;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import tn.esprit.spring.services.EntrepriseServiceImpl;

@Aspect
@Component
public class LoggingAspect {

	
	private static final Logger logger = Logger.getLogger(EntrepriseServiceImpl.class);

	@Before("execution(* tn.esprit.spring.services.*.*(..))")
	public void logMethodEntry(JoinPoint joinPoint) {
	String name = joinPoint.getSignature().getName();
	logger.info("In method " + name + " : ");
	}
	
	@After("execution(* tn.esprit.spring.services.*.*(..))")
	public void logMethodExit(JoinPoint joinPoint) {
		String name = joinPoint.getSignature().getName();
		logger.info("Exiting method " + name + " : ");
	}
	
//	@Around("execution(* tn.esprit.service.*.*(..))")
//	public void logMethodAround(JoinPoint joinPoint) {
//		String name = joinPoint.getSignature().getName();
//		logger.info("Entering and exiting method " + name + " : ");
//	}
//	
	
}
