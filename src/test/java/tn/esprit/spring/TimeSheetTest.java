package tn.esprit.spring;

import tn.esprit.spring.entities.*;
import tn.esprit.spring.services.*;
import tn.esprit.spring.repository.*;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TimeSheetTest {

	@Autowired
	TimesheetServiceImpl timesheetServiceImpl;
	@Autowired
	MissionRepository missionRepository;

	private Mission mission;
	private int idMission;
	private int idEmployee;
	List<Employe> employeeList;
	List<Mission> missionList;

	private static final Logger logger = Logger.getLogger(TimeSheetTest.class);
	
	@Before
	public void init() {
		mission = new Mission("Mohamed Mahdi", "Jendoubi");
		idMission = timesheetServiceImpl.ajouterMission(mission);
		employeeList = timesheetServiceImpl.getAllEmployeByMission(idMission);
		missionList = timesheetServiceImpl.findAllMissionByEmployeJPQL(idEmployee);
	}

	@After
	public void restart() {
		logger.info("Timesheet test finished: Cleanring test data...");
		missionRepository.delete(mission);
		logger.info("TEST DATA CLEARED");
	}

	@Test
	public void testAjouterMission() {
		logger.info("Begin testAjouterMission method");
		Assert.assertEquals(idMission, timesheetServiceImpl.ajouterMission(mission));
		logger.info("End of testAjouterMission method");
	}

	@Test
	public void testgetAllEmployeByMission() throws ParseException {
		logger.info("Begin testgetAllEmployeByMission method");
		logger.debug("Test data employee list: " + employeeList);
		logger.debug("Service return employee list: " + timesheetServiceImpl.getAllEmployeByMission(idMission));
		assertEquals(employeeList, timesheetServiceImpl.getAllEmployeByMission(idMission));

		if ( employeeList.equals(timesheetServiceImpl.getAllEmployeByMission(idMission))) {
			logger.info("testgetAllEmployeByMission SUCCESS");
		} else {
			logger.warn("testgetAllEmployeByMission FAILED");
		}
		logger.info("End of testgetAllEmployeByMission method");
	}

	@Test
	public void testfindAllMissionByEmployeJPQL() throws ParseException {
		logger.info("Begin testfindAllMissionByEmployeJPQL method");
		logger.debug("Test data missions list: " + missionList);
		logger.debug("Service return mission list: " + timesheetServiceImpl.findAllMissionByEmployeJPQL(idMission));

		assertEquals(missionList, timesheetServiceImpl.findAllMissionByEmployeJPQL(idEmployee));
		if ( employeeList.equals(timesheetServiceImpl.findAllMissionByEmployeJPQL(idMission))) {
			logger.info("testfindAllMissionByEmployeJPQL SUCCESS");
		} else {
			logger.warn("testfindAllMissionByEmployeJPQL FAILED");
		}
		logger.info("End of testfindAllMissionByEmployeJPQL method");
	}

}
